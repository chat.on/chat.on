'use strict';

//importation des modules express, axios et body-parser
const express = require('express');
const axios = require('axios');
const bodyParser = require('body-parser');

const urlEncodedParser = bodyParser.urlencoded({ extended: false });
const URL = 'http://localhost:3000';
const server = express();

server.use(express.static(__dirname + '/static'));

//Utilisation du moteur de templates ejs pour créer les routes 
//permettant d'afficher les pages html 
//(et remplacement de l'extension .html par .ejs)
server.set('view engine', 'ejs');

//Affichage du formulaire (création d'un nouvel utilisateur)
server.get('/user/new', (req, res) => {
    res.render('pages/form');
    //on affiche le formulaire sur le port locahost:8050/user/new
});

//Utilisation d'axios pour envoyer les données 
//depuis le serveur web (server.js) à l'api (app.js)

//Méthode post pour créer un nouvel utilisateur
server.post('/user/new', urlEncodedParser, (req, res) => {
    axios.post(URL + '/user/new', req.body)
        .then((response) => {
            console.log('les données du formulaire sont transmises à l\'api');
            console.log(response);
            res.render('pages/form');
            //res.end();
        })
        .catch((error) => {
            if(error) throw error;
            //res.end();
            res.render('pages/form');
        });
});

//Méthode GET pour afficher la liste des utilisateurs
server.get('/users', (req, res) => {
    axios.get(URL + '/users')
        .then((response) => {
        console.log(response.data);
        let users = response.data;
        res.render('pages/userlist', { users});
        })
        .catch((error) => {
            if (err) throw err;
            res.send();
        });
});

//Méthode POST pour effacer un utilisateur
server.post('/users', urlEncodedParser, (req, res) => {
    const deletedId = req.body.deletedId;
    // res.send();
    console.log('Id de l\'utilisateur envoyée');
    console.log(deletedId);
    axios.delete(URL + '/users/'+ deletedId)
    .then((response) => {
        axios.get(URL + '/users')
            .then(response => {
                let users = response.data;
                res.render('pages/userlist', { users });
            })
            .catch((err) => {
                if (err) throw err;
                res.send();
            });
    })
        .catch((err) => {
            if (err) throw err;
            res.send();
        });
})

server.listen(8050);