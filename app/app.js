'use strict';

//importation des modules mysql, express, et body-aparser
const mysql =require('mysql');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.json());

//Connection à la base de données chat
var mysqlConnection = mysql.createConnection({
    host:'localhost',
    user:'root',
    password: 'pwd',
    database: 'chat'
});

mysqlConnection.connect((err) => {
    if(!err){
        console.log('connection succeded')
    }else{
        console.log('connection failed \n Error : ' + JSON.stringify(err, undefined,2) )
    }
});


app.listen(3000, () => {
    console.log('The api server is running');
});


//Création d'un nouvel utilisateur
app.post('/user/new', (req, res) => {
    var newUser = req.body;
    console.log(newUser);
    //res.send(newUser)
    var sql = "INSERT INTO users (user_fname, user_lname, user_pseudo, user_email) VALUES (?, ?, ?, ?)";
//    var sql = "INSERT INTO users (user_fname, user_lname, user_pseudo, user_email) VALUES (name, forename, username, email)";
    
    mysqlConnection.query(sql, [newUser.name, newUser.forename, newUser.username, newUser.email], (err, rows, fields) => {
        if (!err) {
            // console.log(rows);
            res.json(rows);
        } else {
            console.log(err);
        }
    });
});


//Récupération de tous les utilisateurs
app.get('/users', (req, res) => {
    mysqlConnection.query('SELECT * FROM users', (err, rows, fields) => {
        if(!err){
            console.log(rows);
            res.send(rows);
        }else{
            console.log(err);
        }
    });
});

//Effacer un utilisateur de la base de données
app.delete('/users/:user_id', (req, res) => {
    console.log('it passed');
    console.log(req.params.user_id);
    mysqlConnection.query('DELETE FROM users WHERE user_id = ?', [req.params.user_id], (err, rows, fields) => {
        if (!err) {
            // console.log(rows);
            res.send('Deleted successfully');
            console.log('hourra');
        } else {
            console.log(err);
        }
    });
});
